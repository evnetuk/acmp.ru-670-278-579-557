import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Task557 {
    public static int[][] multiply(int[][] a, int[][] b) {
    int m1 = a.length;
    int n1 = a[0].length;
    int m2 = b.length;
    int n2 = b[0].length;
    if (n1 != m2) throw new RuntimeException("Illegal matrix dimensions.");
    int[][] c = new int[m1][n2];
    for (int i = 0; i < m1; i++)
        for (int j = 0; j < n2; j++)
            for (int k = 0; k < n1; k++)
                c[i][j] += a[i][k] * b[k][j];

    return c;


}

    public static void main(String[] args) throws IOException {
        FileReader reader = new FileReader("input.txt");
        List<String> alllLines= Files.readAllLines(Paths.get("C:/Work/GP_test/input.txt"));
      //  int length=Integer.valueOf(alllLines.get(0-1));

        for(String line : alllLines) {
            if(line.isEmpty()){
                System.out.println("NEXT matrix");
                continue;
            }
            int[] arrayInLine = Arrays.stream(line.split(" ")).mapToInt(Integer::parseInt).toArray();
            int mass3[2][2];
            for(int i=0;i<3;i++){
                for(int j=0;j<1;j++){
                    mass3[j+1][i]=arrayInLine[i];
                    mass3[j][i]=arrayInLine[i];
                }}
            System.out.println(Arrays.toString(arrayInLine));
        }
        //System.out.println(multiply());


    }
}
