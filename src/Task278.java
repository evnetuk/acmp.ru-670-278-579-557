import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task278 {
    Scanner in;
    PrintWriter out;
    public static void main(String[] argv) throws FileNotFoundException {
        new Task278().run();
    }

    public void run() throws FileNotFoundException {
        in = new Scanner(new File("input.txt"));
        String s = in.nextLine(), t = in.nextLine() ;
        out = new PrintWriter(new File("output.txt"));
        char arrayS[] = s.toCharArray();
        char arrayT[] = t.toCharArray();
        int i = 0;
        int j = 0;
        int o = 0;
        int k = arrayS.length;
        int z = arrayT.length;
        if (k > z) {
            out.print("NO");
            out.close();
        }
        else {
            while (o!= k){
                while ( arrayT[j]!= arrayS[i]){
                    j++;
                    if (j == z){
                        break;
                    }
                    if (i == k){
                        break;
                    }
                }
                if ( j == z  ){
                    break;}

                o++;
                i++;
                j++;
                if (i == k){
                    break;
                }
            }
            if (o == arrayS.length){
                out.print("YES");
                out.close();
            }
            else {
                out.print("NO");
                out.close();
            }
        }
    }
}